# The colours of Guadalupe

Repository to store data analyses for an in-prep paper:

### "Repeated evolution of blanched coloration in a lizard adapting to independent white-sand habitats"

DOI, link and full citation will be presented here once the paper is published.

## Build the docker image:

``` bash
docker build -t stunts/the-colours-of-guadalupe ./
```

### Alternatively, get the image from docker hub:

``` bash
docker pull stunts/the-colours-of-guadalupe:07
```

## Running:

Move all files under `metafiles` to the input directory and then run:
```bash
docker run -v /path/to/my/input/directory:/snakemake/inputs -v /path/to/my/output/dir:/snakemake/outputs -e "INPUT=inputs/my_file.fasta" -e "OUTPUTDIR=outputs" -e "LTRIM=<LEFT TRIM VALUE>" -e "RTRIM=<RIGHT TRIM VALUE>" -e "CORES=<NUMBER OF CORES>" -e "OUTGROUP=Sceloporus_undulatus" -e "NGEN=1000000" stunts/the-colours-of-guadalupe:07 /opt/conda/envs/snakemake/bin/snakemake --cores all -w 120
```


# Results:

Get your results from the "outputs" directory.


## Commands used for the paper data analysis:

### ND4:
`docker run -v $PWD/inputs:/snakemake/inputs -v $PWD/outputs:/snakemake/outputs -e "INPUT=inputs/Holbrookia_ND4_v5.fna" -e "OUTPUTDIR=outputs" -e "LTRIM=75" -e "RTRIM=192" -e "CORES=6" -e "OUTGROUP=Sceloporus_undulatus" -e NGEN="1500000" -e BOOT="1000" stunts/the-colours-of-guadalupe:07 /opt/conda/envs/snakemake/bin/snakemake --cores all -w 120`

### Mc1r:
`docker run -v $PWD/inputs:/snakemake/inputs -v $PWD/outputs:/snakemake/outputs -e "INPUT=inputs/Holbrookia_Mc1r_v5.fna" -e "OUTPUTDIR=outputs" -e "LTRIM=154" -e "RTRIM=31" -e "CORES=6" -e "OUTGROUP=Sceloporus_undulatus" -e NGEN="1500000" -e BOOT="1000" stunts/the-colours-of-guadalupe:07  /opt/conda/envs/snakemake/bin/snakemake --cores all -w 120`

# Data:

You can find the untrimmed FASTA files used in the paper inside the `metafiles` directory.

You can find the alignments used in the paper inside the `alignments` directory. Note that running the pipeline will also generate these files.
