import re

inputfile = os.environ.get("INPUT")
outputdir = os.environ.get("OUTPUTDIR")
left_trim = os.environ.get("LTRIM")
right_trim = os.environ.get("RTRIM")
num_cores = os.environ.get("CORES")
outgroup = os.environ.get("OUTGROUP")
ngen = os.environ.get("NGEN")
nboot = os.environ.get("BOOT")
#Usage: LTRIM="70" RTRIM="74" OUTPUTDIR="outputs" INPUT="inputs/file.fasta" CORES="16" OUTGROUP="Sceloporus_undulatus" NGEN="1000000" snakemake --cores all -w 120

input_basename = os.path.basename(re.sub("\..*?$", "", inputfile))
output_basename = f'{outputdir}/{input_basename}'
output_fullpath = os.path.abspath(outputdir)
if ngen is None:
    ngen = 150000
if nboot is None:
    nboot = 100

rule all:
    input:
        f'{output_basename}_aligned.fasta',
        f'{output_basename}_aligned_trimmed.fasta',
        f'{outputdir}/RAxML_bipartitions.{input_basename}',
        f'{output_basename}_aligned_trimmed.nex',
        f'{output_basename}_aligned_trimmed_block.nex',
        f'{output_basename}.con.tre',
        f'{output_basename}_sanitized.con.tre',
        f'{output_basename}_c.svg',
        f'{output_basename}_p.svg'
    
    message:
        'Run sucessfull. Your phylogeny is ready. Look at the pretty colours!'

rule clean:
    shell:
        'rm -rf outputs/*'

rule alignment:
    input:
        inputfile
    output:
        '{output_basename}_aligned.fasta'
    shell:
        'mafft --auto {input} > {output}'

rule trimming:
    input:
        f'{output_basename}_aligned.fasta'
    output:
        f'{output_basename}_aligned_trimmed.fasta'
    shell:
        'python3 scripts/trimmer.py {input} {left_trim} {right_trim} > {output}'
        
rule raxml:
    input:
        f'{output_basename}_aligned_trimmed.fasta'
    output:
        f'{outputdir}/RAxML_bipartitions.{input_basename}'
    run:
        cmd = f'raxmlHPC-PTHREADS-AVX -f a -m GTRCAT -p 112358 -x 112358 -N {nboot} -s {input} -n {input_basename} -w {output_fullpath} -T {num_cores}'
        if outgroup is not None:
          cmd += f' -o "{outgroup}"'
        shell(cmd)

rule fasta2nexus:
    input:
        f'{output_basename}_aligned_trimmed.fasta'
    output:
        f'{output_basename}_aligned_trimmed.nex'
    shell:
        'python scripts/converter.py {input} {output}'

rule nexus_block:
    input:
        f'{output_basename}_aligned_trimmed.nex'
    output:
        f'{output_basename}_aligned_trimmed_block.nex'
    run:
        block_elements = ["Begin mrbayes;", f"Outgroup {outgroup.replace('|', '+')};",
                          f"mcmcp ngen={ngen} printfreq=1000 samplefreq=100 nchains=4 savebrlens=yes filename={input_basename};",
                          "mcmc;",
                          f"sumt filename={input_basename} conformat=simple contype=halfcompat;",
                          "end;\n"]
        if outgroup is None:
            block_elements.remove(block_elements[1])
        block_string = "\n".join(block_elements)
        infile = str(input)
        outfile = str(output)
        nexus_in_handle = open(infile, "r")
        nexus_out_handle = open(outfile, "w")
        verify = True
        for lines in nexus_in_handle:
            if verify:
                if lines.startswith("format datatype"):
                  lines = lines.replace("missing=?", "missing=n")
                  verify = False
            else:
              lines = lines.replace("|", "+")
            nexus_out_handle.write(lines)
        nexus_out_handle.write(block_string)
        nexus_in_handle.close()
        nexus_out_handle.close()

rule mrbayes:
    input:
        f'{output_basename}_aligned_trimmed_block.nex'
    output:
        f'{output_basename}.con.tre'
    shell:
        'cd outputs && mb ../{input}' 

rule sanitize_names:
    input:
        f'{output_basename}.con.tre'
    output:
        f'{output_basename}_sanitized.con.tre'
    shell:
        'sed "s/\+/|/g" {input} > {output}'

rule draw_tree:
    input:
        [f'{outputdir}/RAxML_bipartitions.{input_basename}',
         f'{output_basename}_sanitized.con.tre',
         f'inputs/colour_codes.json']
    output:
        [f'{output_basename}_c.svg',
         f'{output_basename}_p.svg']
    run:
        shell('python scripts/treeplotter.py {input[0]} {input[1]} {input[2]} {output[0]} c')
        shell('python scripts/treeplotter.py {input[0]} {input[1]} {input[2]} {output[1]} p')
