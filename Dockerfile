FROM snakemake/snakemake:v6.15.5

# apt stuff
RUN apt update
RUN apt install -y raxml mrbayes mafft gcc

# Pypi stuff
RUN pip install biopython
RUN pip install toytree

# Patch toyplot for python 3.10
RUN sed -i "s/collections.Sequence/collections.abc.Sequence/" /opt/conda/envs/snakemake/lib/python3.10/site-packages/toyplot/color.py

WORKDIR /snakemake
COPY scripts ./scripts
COPY ./Snakefile ./
