#!/usr/bin/env python3

import sys
import re


def fasta_parser(target_file):
    """
    Parses a FASTA file and retruns a dict with {title: seq}
    """
    target_handle = open(target_file, 'r')
    sequence_data = {}

    for lines in target_handle:
        if lines.startswith(">"):
            name = lines[1:].rstrip()
            sequence_data[name] = ""
        else:
            sequence_data[name] += lines.rstrip().lower()

    target_handle.close()
    return sequence_data


def fasta_writer(seq_dict, ltrim, rtrim):
    """
    Writes a FASTA file from a sequence dict {title: seq}
    Also trimms the left and right ends by a defined ammount
    """
    def _convert_to_n(match_obj):
        if match_obj.group() is not None:
            return match_obj.group().replace("-", "n")

    for k, v in seq_dict.items():
        print(f'>{k}')
        v = v[int(ltrim):-int(rtrim)]
        v = re.sub("^-+", _convert_to_n, v)
        v = re.sub("-+$", _convert_to_n, v)
        print(v)


if __name__ == "__main__":
    # Usage: python3 trimmer.py input_file.fasta left_trim[int] right_trim[int]
    SEQ_DICT = fasta_parser(sys.argv[1])
    fasta_writer(SEQ_DICT, sys.argv[2], sys.argv[3])
