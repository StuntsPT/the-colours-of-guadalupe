#!/usr/bin/env python3
"""
This module plots phylogenetic trees from a
RAxML `bipartitions` output file.
"""

import itertools
import json
from sys import argv
import toytree
import toyplot
import toyplot.svg

# Set a palette and bootstrap color intervals
# from https://colorbrewer2.org/#type=diverging&scheme=Spectral&n=7
#COL_PALETTE = ['rgb(213,62,79)','rgb(252,141,89)','rgb(254,224,139)',
#              'rgb(255,255,191)','rgb(153,213,148)','rgb(26,152,80)',
#              'rgb(50,136,189)']
#
#BOOT_INTERVALS = (30, 20, 20, 10, 10, 10, 1)

COL_PALETTE = ['#2a581d', '#e4f2cb', '#7bb142', '#2a581d']
BOOT_INTERVALS = (1, 50, 30, 20)


def info_gatherer(tree_file, groups_file):
    """
    Gathers information from the input files:
    `tree_file` is the path to a RAxML `bipartitions` file and
    `groups_file` is the path to a JSON file containing group information for
    both taxa colours and rooting.
    Retuns a toytree.tree() object containing the basic tree as read from
    the input file and a dict containing group information based on taxa
    prefixes.
    """
    my_tree = toytree.tree(tree_file)
    try:
        with open(groups_file) as json_data:
            groups_data = json.load(json_data)
    except json.decoder.JSONDecodeError:
        print("Invalid JSON detected. Ignoring user defined groups.")
        grp_colrs = None
    else:
        try:
            grp_colrs = groups_data["group_colors"]
        except KeyError:
            print("No group information found in JSON - all taxa will be drawn"
                  " in black.")
            grp_colrs = None
            my_tree = my_tree.ladderize(1)

        try:
            my_tree = my_tree.root(regex=f"({'|'.join(groups_data['root_groups'])}).*").ladderize(1)

        except KeyError:
            print("No rooting information found in JSON - rooting will be left"
                  " at first taxon.")
            my_tree = my_tree.ladderize(1)

    return my_tree, grp_colrs


def group_finder(tre):
    """
    Finds groups with "high" PP found in bayesain trees, and returns them,
    along with the respective PP values
    """
    group_support = []
    for idx, value in zip(tre.get_node_values("idx"),
                          tre.get_node_values("support")):
        if value != "":
            tlabs = tre.get_tip_labels(idx=int(idx))
            if "." in value:
                value = str(round(float(value), 2))
                if len(value) < 4:
                    value += "0"
            group_support.append((tlabs, value))

    return (group_support)


def tree_plotter(tre, group_colours, save_location, tt, extra_support):
    """
    Plots the trees from the input objects
    `tre` is a toytree.tree() object
    `group_colours` is a dict where the keys are taxonomic group prefixes
    and the values are the respective colours
    The third argument, `save_location` is the path to where the resulting
    SVG file should be written.
    """
    node_palette = [COL_PALETTE[idx:idx+1] * interval
                    for idx, interval in zip(range(len(COL_PALETTE)),
                                             BOOT_INTERVALS)]
    node_palette = list(itertools.chain.from_iterable(node_palette))
    
    colors = [node_palette[int(i)] if i != "" else "#e4f2cb" for i in tre.get_node_values('support')]


    # Deal with bootstrap values

    # Add support values from a second tree (usually obtained from a different method)
    bootstraps = tre.get_node_values("support")
    bootstraps[1] = "100"  # Place root support value
    for groups in extra_support:
        node_value = tre.get_mrca_idx_from_tip_labels(names=groups[0])
        taxa = tre.get_tip_labels(idx=int(node_value))
        if set(taxa) == set(groups[0]):
            node_list_index = list(tre.get_node_values("idx")).index(str(node_value))
            bootstraps[node_list_index] += f"/{groups[1]}"  # TODO: must change to have bootstrap text due to the code below...
    # Remove bootstrap values if below 50%
    new_bootstraps = []
    for value in bootstraps:
        try:
            if int(value) >= 50:
                new_bootstraps.append(value)
            else:
                new_bootstraps.append("")
        except(ValueError):
            new_bootstraps.append(value)
    bootstraps = []
    for value in new_bootstraps:
        if value != "":
            try:
                value = value.split("/")[1]
            except(IndexError):
                value = "-"
        bootstraps.append(value)

    # Make very short branches visible
    my_dist = []
    for i in tre.get_node_values("dist"):
        if i != "":
            if float(i) < 1e-5:
                i = 2e-4
        my_dist.append(i)

    tre = tre.set_node_values(feature="dist",
                              values={k: v for k, v in zip(tre.get_node_values("idx"), my_dist) if k != ""})

    # Reduce root distance
    ingroup_idx = tre.get_mrca_idx_from_tip_labels(regex=".*\d")
    root_idx = tre.get_mrca_idx_from_tip_labels(regex="^Sce.*")
    tre = tre.set_node_values(feature="dist", values={ingroup_idx: 0.005, root_idx: 0.005}).ladderize(1)
    
    # Paint tip labels
    try:
        tip_colorlist = []
        for tip in tre.get_tip_labels():
            for k, v in group_colours.items():
                if k in tip:
                    tip_colorlist.append(v)
                    break
            else:
                tip_colorlist.append("#000000")
    except AttributeError:
        tip_colorlist = "#000000"

    if tt == "p":
        EL = True
    else:
        EL = False
    canvas, axes, mark = tre.draw(width=1300, tip_labels_align=False,
                                  scalebar=EL,
                                  use_edge_lengths=EL,
                                  tree_style="n",
                                  tip_labels_colors=tip_colorlist,
                                  edge_widths=3,
                                  node_labels=bootstraps,
                                  node_markers="o", node_sizes=28,
                                  node_style={"stroke": "black",
                                              "stroke-width": 0.75},
                                  node_colors=colors,
                                  height=len(tre.get_tip_labels()) * 20,
                                  tip_labels_style={"font-weight": "bold"},
                                  node_labels_style={"font-size": "12px",
                                                     "font-weight": "bold",
                                                     "fill": "white"}
                                  )
    
    empty_marker = [toyplot.marker.create(shape="-")]
    markers = [toyplot.marker.create(shape="o", size=24,
                                     mstyle={"fill": x}) for x in COL_PALETTE[1:]]
    markers = empty_marker + markers
    legend_markers = list(zip(['Bootstrap ≤ 50 and PP ≤ 0.5',
                               'Bootstrap ≤ 50 and PP ≥ 0.51',
                               'Bootstrap ≥ 51 and ≤ 80',
                               'Bootstrap ≥ 81'], markers))

    canvas.legend(legend_markers,
                  corner=("top-left", 50, 170, 140),
                  label="Branch support")
    toyplot.svg.render(canvas, save_location)


if __name__ == "__main__":
    # Usage: python treeplotter.py /path/to/tree_to_draw (raxml or mrbayes)
    # /path/to/tree_to_get_support (raxml or mrbayes) /path/to/file.json
    # /path/to/output.svg tree_type (c or p)
    TREE_TYPE = argv[5]
    tree, groups = info_gatherer(argv[1], argv[3])
    sup_tree, _ = info_gatherer(argv[2], argv[3])
    g_support = group_finder(sup_tree)
    tree_plotter(tree, groups, argv[4], TREE_TYPE, g_support)
